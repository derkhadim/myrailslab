class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.date :datevent
      t.text :adress

      t.timestamps null: false
    end
  end
end
