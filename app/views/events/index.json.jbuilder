json.array!(@events) do |event|
  json.extract! event, :id, :name, :datevent, :adress
  json.url event_url(event, format: :json)
end
